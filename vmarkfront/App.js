import './App.scss';
import Page from './components/Page.jsx';

function App() {
  return (
    <div className="App">
      <Page/>
    </div>
  );
}

export default App;
