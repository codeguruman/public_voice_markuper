class CallStats {
    callsProcessed = 10;
    callsCount = 20;
    fragmentsProcessed = 25;
    fragmentsCount = 30;
    goodCount = 40;
    badCount = 50;
    neutralCount = 60;
}

module.exports = CallStats; 