import React from 'react';
import { useEffect } from 'react';
import Stats from './UI/stats.jsx';
import Actions from './UI/actions.jsx';
import Player from './UI/player.jsx';
import CallStats from '../structures/callStats';

export default function Page (props) {
    const stats = new CallStats();
    const main_url = '';
    const token = '';
    
    const [callsState, setCallsState] = React.useState(null);
    const [linkState, setLinkState] = React.useState(null);
    const [currentCallState, setCurrentCallState] = React.useState(0);
    const [pageState, setPageState] = React.useState(stats);

    
    const getLink = (main_url, token, items, idx) => {
        
        if (items == null) {
            return '';
        }

        let filename = items[idx].fileName;
        let name = filename.split('__')[1];
        return main_url + 'Download?filename=' + name + '&apikey=' + token;
    }

    const getCallsStates = async(main_url, token) => {
            fetch(main_url + 'GetProcessingStates?apikey=' + token)
            .then(response => response.json())
            .then(data => {
                setCallsState(data);
                setLinkState(getLink(main_url, token, data, 0));
                
            });
    };

    const propsValues = {
        title: "callIds",
        id: "q1"
    };

    const nextCall = () => {
        setCurrentCallState(currentCallState + 1)
        setPageState({...pageState, fragmentsProcessed: 0, callsProcessed: pageState.callsProcessed + 1});
        setLinkState(getLink(main_url, token, callsState, currentCallState));
    }

    const updatePage = (value) => {
        // console.log(value);
        switch(value) {
            case "bad": 
                setPageState({...pageState, badCount : pageState.badCount + 1, fragmentsProcessed: pageState.fragmentsProcessed + 1});
                break;
            
            case "good": 
                setPageState({...pageState, goodCount : pageState.goodCount + 1, fragmentsProcessed: pageState.fragmentsProcessed + 1});
                break;

            case "neutral":
                setPageState({...pageState, neutralCount : pageState.neutralCount + 1, fragmentsProcessed: pageState.fragmentsProcessed + 1});
                break;
            
            default:
                break;
        }
    }

    useEffect(() => {getCallsStates(main_url, token)}, []);

    return (
        <div>
            <Player data={linkState}/>
            <Actions data={propsValues} update={updatePage} />
            <button className="next-button" onClick={nextCall}> Next call </button>
            <Stats data={pageState} />
        </div>
    )
}