import React from 'react';
 
export default function Stats(props) {
    return(
        <ul className="stats-list">
            <li>Обработано звонков {props.data.callsProcessed} из {props.data.callsCount}</li>
            <li>Обработано фрагментов {props.data.fragmentsProcessed} из {props.data.fragmentsCount}</li>
            <li><span className='icon smile-good'></span> : {props.data.goodCount}</li>
            <li><span className='icon smile-neutral'></span> : {props.data.neutralCount}</li>
            <li><span className='icon smile-bad'></span> : {props.data.badCount}  </li>
        </ul>
    );
}