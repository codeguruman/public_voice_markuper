import MyButton from './myButton';
import React, { useState } from 'react';
 
export default function Actions(props) {
    const [callResult, setCallResult] = useState({ id : props.data.id, value : ''});

    const saveCommand = (emotion) => {
        setCallResult({...callResult, value: emotion});
    }

    const onChangeValue = (e) => {
        saveCommand(e.target.value);
        props.update(e.target.value);
    }

    return(
        <div className="actions" onClick={onChangeValue}>
            <MyButton value={"good"}/>
            <MyButton value={"bad"}/>
            <MyButton value={"neutral"}/>
        </div>
    );
}