import react from 'react';
 
export default function Player(props) {
    return(
        <div>
           <audio controls src={props.data}></audio>
        </div>
    );
}