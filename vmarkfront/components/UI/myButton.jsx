import React from 'react'

export default function MyButton({value}) {

    return (
        <button className={"icon smile-" + value} value={value}/>
    )
}
