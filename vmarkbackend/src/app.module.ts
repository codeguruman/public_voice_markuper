import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VoiceMarkupModule } from './voicemarkup/voiceMarkupModule';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    VoiceMarkupModule,
    //MongooseModule.forRoot('mongodb://localhost:27017/nest')
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGODB_URI'),
      }),
      inject: [ConfigService],
    }),
  ],
})
export class AppModule {}
