/* eslint-disable prettier/prettier */
import { Controller, Get, Post, Body, Res, HttpStatus, Query } from '@nestjs/common';
import { VoiceMarkupService } from './voiceMarkupService';
import { MarkupBatch } from './voiceMarkupService';
import { MarkupItem } from '../schema/voicemarkup.schema';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { Response } from 'express';

@Controller('/api')
export class VoiceMarkupController {
  constructor(private readonly appService: VoiceMarkupService) { }

  @Get('/getItems')
  @ApiTags('voiceitems')
  @ApiOperation({ summary: 'Getting all markup voice items' })
  async getMarkupItems(@Res() response: Response, @Query('apikey') apikey: string): Promise<MarkupItem[]> {
    if (!this.appService.isValidKey(apikey)) {
      response.status(HttpStatus.FORBIDDEN).send('You are not allowed to do that');
      return;
    }
    response.status(HttpStatus.OK).send(await this.appService.getMurkupItems());
    return;
  }

  @Post('/additems')
  @ApiTags('voiceitems')
  @ApiOperation({ summary: 'Add markup voice items' })
  async AddMarkupItems(@Body() markupBatch: MarkupBatch,
    @Res() response: Response,
    @Query('apikey') apikey: string) {
    if (!this.appService.isValidKey(apikey)) {
      response.status(HttpStatus.FORBIDDEN).send('You are not allowed to do that');
      return;
    }

    await this.appService.addMurkupBatch(markupBatch);
    response.status(HttpStatus.OK).send('Ok');
    return;
  }
}
