/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { VoiceMarkupService } from './voiceMarkupService';
import { VoiceMarkupController } from './voiceMarkupController';
import { MongooseModule } from '@nestjs/mongoose';
import { MarkupItem, MarkupItemSchema } from '../schema/voicemarkup.schema';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MarkupItem.name, schema: MarkupItemSchema },
    ]),
    ConfigModule.forRoot(),
  ],
  providers: [VoiceMarkupService, ConfigService],
  controllers: [VoiceMarkupController],
})
export class VoiceMarkupModule { }
