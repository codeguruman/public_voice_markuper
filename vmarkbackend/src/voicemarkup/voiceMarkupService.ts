/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { MarkupItem } from '../schema/voicemarkup.schema';
import { CreateMarkupItemDto } from '../schema/createMarkupItemDTO.dto';
import { UpdateMarkupItemDto } from '../schema/updateMarkupItemDTO.dto';

export class MarkupBatch {
  @ApiProperty({ type: [MarkupItem] })
  items: MarkupItem[];
}

@Injectable()
export class VoiceMarkupService {
  constructor(
    @InjectModel(MarkupItem.name) private model: Model<MarkupItem>,
    private configService: ConfigService,
  ) { }

  async getMurkupItem(id: string): Promise<MarkupItem> {
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
      return null;
    }
    const markupItem = await this.model.findById({ _id: id }).exec();
    return markupItem;
  }

  async getMurkupItems(): Promise<MarkupItem[]> {
    const items = await this.model.find().exec();
    return items;
  }

  async addMurkupBatch(batch: MarkupBatch) {
    for (const item of batch.items) {
      await this.addMurkupItem(item);
    }
  }

  async addMurkupItem(item: MarkupItem): Promise<MarkupItem> {
    const existingItem = await this.getMurkupItem(item.id);

    if (existingItem == null) {
      const dto = new CreateMarkupItemDto();
      dto.mapFromMarkupItem(item);
      return await this.createMurkupItem(dto);
    } else {
      const dto = new CreateMarkupItemDto();
      dto.mapFromMarkupItem(item);
      return await this.updateMurkupItem(dto, item.id);
    }
  }

  isValidKey(api_key: string) {
    const real_key = this.configService.get<string>('API_KEY');
    return real_key == api_key;
  }

  private async createMurkupItem(
    createMarkupItemDto: CreateMarkupItemDto,
  ): Promise<MarkupItem> {
    const newItem = await this.model.create(createMarkupItemDto);
    return newItem.save();
  }

  private async updateMurkupItem(
    updateMarkupItemDto: UpdateMarkupItemDto,
    id: string,
  ): Promise<MarkupItem> {
    const editedItem = await this.model.findByIdAndUpdate(
      { _id: id },
      updateMarkupItemDto,
    );
    return editedItem;
  }
}
