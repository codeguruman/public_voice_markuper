import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { VoiceMarkupModule } from './voicemarkup/voiceMarkupModule';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const document = SwaggerModule.createDocument(
    app,
    new DocumentBuilder()
      .setTitle('Voice markup backend')
      .setDescription('Web API for handling markuped voice data')
      .setVersion('1.0')
      .addTag('voiceitems')
      .build(),
    { include: [VoiceMarkupModule] },
  );

  SwaggerModule.setup('api', app, document);
  app.enableCors();
  await app.listen(3021);
}
bootstrap();
