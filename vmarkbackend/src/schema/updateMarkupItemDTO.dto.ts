/* eslint-disable prettier/prettier */
import { PartialType } from '@nestjs/mapped-types';
import { CreateMarkupItemDto } from './createMarkupItemDTO.dto';

export class UpdateMarkupItemDto extends PartialType(CreateMarkupItemDto) { }
