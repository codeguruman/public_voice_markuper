/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class MarkupItem {
  @ApiProperty()
  @Prop()
  id: string;

  @ApiProperty()
  @Prop()
  fileName: string;

  @ApiProperty()
  @Prop()
  startingSecond: number;

  @ApiProperty()
  @Prop()
  endingSecond: number;

  @ApiProperty({
    description: 'value in (good|bad|neutral)'
  })
  @Prop()
  value: string;
}

export const MarkupItemSchema = SchemaFactory.createForClass(MarkupItem);