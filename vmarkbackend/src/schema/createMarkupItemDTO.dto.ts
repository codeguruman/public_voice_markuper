/* eslint-disable prettier/prettier */
import { MarkupItem } from '../schema/voicemarkup.schema';

export class CreateMarkupItemDto {
    fileName: string;
    startingSecond: number;
    endingSecond: number;
    value: string;

    mapFromMarkupItem(markupItem: MarkupItem) {
        this.fileName = markupItem.fileName;
        this.startingSecond = markupItem.startingSecond;
        this.endingSecond = markupItem.endingSecond;
        this.value = markupItem.value;
    }
}